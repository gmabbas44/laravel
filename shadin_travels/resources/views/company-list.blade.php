@extends('layout')

@section('main_content')


<h1 class="h3 mb-4 text-gray-800">Company List</h1>

<div class="card">
    <div class="card-head">
        <a href="/add-new-company" class="float-right btn btn-primary btn-sm">Add New Company</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Company name</th>
                        <th>Company Code</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $companies as $com )
                    <tr>
                        <td scope="row">{{$com->company_id}}</td>
                        <td>{{$com->company_code}}</td>
                        <td>{{$com->company_name}}</td>
                        <td>{{$com->email}}</td>
                        <td>{{$com->mobile}}</td>
                        <td><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>||<a class="text-danger" href="delete/{{$com->company_id}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@endsection