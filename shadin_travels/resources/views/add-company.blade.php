@extends('layout')

@section('main_content')

<h1 class="h3 mb-4 text-gray-800">Add New Company</h1>
<div class="card">
    <div class="card-body">
        <form action="savecompany" method="POST">
            @csrf
            <div class="row ">
                <div class="col-md mb-4">
                    <input type="text" name="company_code" class="form-control" placeholder="Company Code">
                </div>
                <div class="col-md mb-4">
                    <input type="text" name="company_name" class="form-control" placeholder="Company name">
                </div>
            </div>
            <div class="row ">
                <div class="col-md mb-4">
                    <input type="email" name="email" class="form-control" placeholder="Company email">
                </div>
                <div class="col-md mb-4">
                    <input type="number" name="mobile" min="0" class="form-control" placeholder="Company phone number">
                </div>
            </div>
            <div class="row ">
                <div class="col-md mb-4">
                    <input type="password" name="password" class="form-control" placeholder="Enter password">
                </div>
                <div class="col-md mb-4">
                    <!-- <input type="number" class="form-control" placeholder="Enter pepeat password"> -->
                </div>
            </div>
            <div class="row ">
                <!-- <div class="col-md mb-4">
                    <input class="form-control" type="file">
                </div> -->
            </div>
            <button class="btn btn-primary">Save</button>
        </form>
    </div>
</div>



@endsection