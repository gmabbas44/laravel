<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', function () {
    return view('login');
});

Route::post('loginsubmit', 'App\Http\Controllers\SuperAdmin@loginsubmit');
Route::get('super-admin-deshboard', 'App\Http\Controllers\SuperAdmin@superAdminDeshboard');
Route::get('company-list', 'App\Http\Controllers\SuperAdmin@companyList');
Route::get('add-new-company', 'App\Http\Controllers\SuperAdmin@addNewCompary');
Route::post('savecompany', 'App\Http\Controllers\SuperAdmin@savecompany');
Route::get('delete/{id}', 'App\Http\Controllers\SuperAdmin@delete');

