<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Super_admin;
use App\Models\Companies;

class SuperAdmin extends Controller
{
    public function loginsubmit( Request $req)
    {
        $results = Super_admin::select('*')->where([
            ['mobile', '=', $req->mobile_number],
            ['password', '=', $req->password]
        ])->get();
        
        if ( $results ) {
            return redirect('/super-admin-deshboard');
        }
        else{
            return redirect('/');
        }
    //    return Super_admin::all();
    }

    public function superAdminDeshboard()
    {
        return view('/super-admin-deshboard');
    }

    public function addNewCompary()
    {
        return view('/add-company');
    }
    public function companyList()
    {
        $companies = Companies::all();
        return view('company-list', ['companies'=> $companies]);
        
    }
    public function savecompany( Request $req)
    {
        $com = new Companies();
        $com->company_code = $req->company_code;
        $com->company_name = $req->company_name;
        $com->email = $req->email;
        $com->mobile = $req->mobile;
        $com->password = $req->password;
        $results = $com->save();
        if ( $results ) {
            return redirect('company-list');
        } else {
            return redirect('add-new-company');
        }
    }

    public function delete($id) {
        $res = Companies::destroy(['company_id' => $id]);
        if ( $res ) {
            return redirect('company-list');
        }

    }
}
