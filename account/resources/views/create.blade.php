
@extends('layout')

@section('contain')
<div class="row justify-content-center">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Register</h4>
            <form action="/createsubmit" method="post">
            @csrf
                <div class="form-group">
                    <label for="name">name address</label>
                    <input type="name" name="name" class="form-control" id="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <label for="Email">Email address</label>
                    <input type="email" name="email" class="form-control" id="Email" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection