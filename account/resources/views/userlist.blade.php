

@extends('layout')

@section('contain')
<div class="row justify-content-center">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title text-center">User list</h4>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ( $users as $u )
                    <tr>
                        <td scope="row">{{$u->id}}</td>
                        <td>{{$u->name}}</td>
                        <td>{{$u->email}}</td>
                        <td>{{$u->password}}</td>
                        <td><a href="">Edit</a>|| <a href="">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection