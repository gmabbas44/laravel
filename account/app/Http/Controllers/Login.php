<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class Login extends Controller
{
    public function loginUser()
    {
        return view('userlist');
    }

    public function loginsubmit(Request $res)
    {
        // print_r($res->input());
        User::select('*')->where([
            ['email', '=', $res->email],
            ['password', '=', $res->password],
        ])->get();
        
        $res->session()->put('loginData', [$res->input()]);
        return redirect('/list');
    }
}
