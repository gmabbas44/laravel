<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class Users extends Controller
{
    function list(){
        // return Session::get('loginData');
        $users = User::all();
        return view('userlist', ['users' => $users ]) ;
    }

    function create() {
        return view('create');
    }

    function createsubmit(Request $res)
    {
        $user = new User();
        $user->name = $res->name;
        $user->email = $res->email;
        $user->password = $res->password;
        $results = $user->save();
        if ($results ) {
            return redirect('/list');
        }
    }
}
